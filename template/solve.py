from utils.common import solve_puzzle


def solve(lines):
    part1 = None

    part2 = None

    return part1, part2


debug = True
solve_puzzle(year=2022, day=-1, solver=solve, do_sample=True, do_main=False)
# solve_puzzle(year=2022, day=-1, solver=solve, do_sample=False, do_main=True)
